<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles.css">

        <title>Google Maps Geocoding </title>
    </head>
    <body>
        <form action="" method="POST">
            <input type="text" name="address" placeholder="Enter any address here" />
            <input type="submit" value="Locate! " name="locate" />
        </form>
    </body>
    <?php
    if ($_POST) {
        require_once 'geolocate.php';

        // get latitude, longitude and formatted address
        $data = geocode($_POST['address']);

        // if able to geocode the address
        if ($data) {
            ?>

            <!-- google map will be shown here -->
            <div id="gmap_canvas">Loading map...</div>
            <div id='map-label'>Map shows approximate location.</div>

            <!-- JavaScript to show google map -->
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>    
            <script type="text/javascript">
                function init_map() {
                    var myOptions = {
                        zoom: 14,
                        center: new google.maps.LatLng(<?php echo $data['lat']; ?>, <?php echo $data['lng']; ?>),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
                    marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(<?php echo $data['lat']; ?>, <?php echo $data['lng']; ?>)
                    });
                    infowindow = new google.maps.InfoWindow({
                        content: "<?php echo $data['formatted_address']; ?>"
                    });
                    google.maps.event.addListener(marker, "click", function () {
                        infowindow.open(map, marker);
                    });
                    infowindow.open(map, marker);
                }
                google.maps.event.addDomListener(window, 'load', init_map);
            </script>

            <?php
            // if unable to geocode the address
        } else {
            echo "Ups, no map found!<br /> Please check address and try again.";
        }
    }
    ?>
</html>