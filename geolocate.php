<?php

function geocode($address)
{
    //url encode address
    $address = urlencode($address);

    $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$address}";

    //get the json response and decode
    $json = file_get_contents($url);
    $json = json_decode($json, true);

//    $address = array();
    // if found given address
    if ($json['status'] == 'OK') {
        $formattedAddress = $json['results'][0]['formatted_address'];
        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];

        // check if data is valid 
        if ($formattedAddress && $lat && $lng) {
            $data = array();
            $data['formatted_address'] = $formattedAddress;
            $data['lat'] = $lat;
            $data['lng'] = $lng;

            return $data;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
